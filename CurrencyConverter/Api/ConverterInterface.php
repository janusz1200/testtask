<?php

namespace JanuszWitrykus\CurrencyConverter\Api;

interface ConverterInterface
{
    /**
     * @param float $convertFrom
     * @return float
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function convert($convertFrom);
}
